from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

try:
    from tags.models import Tag
except Exception:
    Tag = None


# Create your views here.
def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)


class TagListView(ListView):
    model = Tag
    context_object_name = "tags"
    template_name = "tags/list.html"


class TagDetailView(DetailView):
    model = Tag
    context_object_name = "tag"
    template_name = "tags/detail.html"


class TagCreateView(CreateView):
    model = Tag
    fields = ["name"]
    template_name = "tags/new.html"
    success_url = reverse_lazy("tags_list")


class TagUpdateView(UpdateView):
    model = Tag
    fields = ["name"]
    template_name = "tags/edit.html"
    success_url = reverse_lazy("tags_list")


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")
